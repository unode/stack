#!/usr/bin/env bash

mkdir tmp
wget https://www.stackage.org/stack/linux-x86_64 -O tmp/down-stack-new.tar.gz
cd tmp || exit 1
tar xf down-stack-new.tar.gz
VERSION="$(/usr/bin/env ls -d stack-*)"
cd .. || exit 1
mv tmp/"${VERSION}" .
rm -f stack-latest
ln -s "${VERSION}" stack-latest

rm -f tmp/down-stack-new.tar.gz
rmdir tmp
